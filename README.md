# RaspberryPi Alpine Ruby

This image is unofficial balena.io alpine Ruby base image for IoT devices. The image is optimized for use with balena.io and balenaOS, but can be used in any Docker environment running on the appropriate architecture.

**Ruby Version** `3.0.1`
